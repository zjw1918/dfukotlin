package cn.zjw.dfukotlin.models

import android.bluetooth.BluetoothDevice

/**
 * Created by lincoln on 17-8-31.
 */
data class MyScannedDevice(val mDevice: BluetoothDevice, var mRssi: Int) {
    override fun equals(other: Any?): Boolean {
        val o = other as MyScannedDevice
        return this.mDevice == o.mDevice
    }
}