package cn.zjw.dfukotlin.services

import android.app.Service
import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import cn.zjw.dfukotlin.configs.MyConfigBle
import cn.zjw.dfukotlin.eventsPojo.MsgBox
import cn.zjw.dfukotlin.eventsPojo.MsgBox.Companion.BLE_EVENT_STOP_SCAN
import cn.zjw.dfukotlin.eventsPojo.MsgBox.Companion.BLE_UI_UPDATE_MENU
import cn.zjw.dfukotlin.models.MyScannedDevice
import com.orhanobut.logger.Logger
import org.greenrobot.eventbus.EventBus


enum class MyConnectionEnum {
    STATE_DISCONNECTED,
    STATE_CONNECTING,
    STATE_CONNECTED
}

object MyBleSingleInfo {
    var mDevice: BluetoothDevice? = null
    var mAddress: String? = null
    var mName: String? = null
    var mDeviceList = arrayListOf<MyScannedDevice>()
    var mConnectionState = MyConnectionEnum.STATE_DISCONNECTED


    fun clear() {
        mDevice = null
//        mName = null
//        mAddress = null
        mDeviceList.clear()
        mConnectionState = MyConnectionEnum.STATE_DISCONNECTED
    }
}

class BleService : Service() {
    private val mBinder: BleBinder = BleBinder()
    private var mBleManager: BluetoothManager? = null
    var mBleAdapter: BluetoothAdapter? = null
    private var mBluetoothGatt: BluetoothGatt? = null
    var mIsScanning = false
    var serv1530: BluetoothGattService? = null
    var chara1531: BluetoothGattCharacteristic? = null

    private val handler = Handler()
    private val r = Runnable {
        if (mIsScanning) {
            scanStop()
        }
    }

    companion object {
        private val SCAN_PERIOD: Long = 10 * 1000
    }

    private val mGattCallback = object: BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    MyBleSingleInfo.mConnectionState = MyConnectionEnum.STATE_CONNECTED
                    Logger.d("onConnectionStateChange: connected")
                    Logger.d("Attempting to start service discovery:")
                    mBluetoothGatt!!.discoverServices()
                }

                BluetoothProfile.STATE_DISCONNECTED -> {
                    MyBleSingleInfo.mConnectionState = MyConnectionEnum.STATE_DISCONNECTED
                    Logger.d("onConnectionStateChange: disconnected")
                    MyBleSingleInfo.clear()
                    publishEventToUpdate(MsgBox.BLE_EVENT_DISCONNECT)
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Logger.d("onServicesDiscovered[ok] received: " + status)
                val services = mBluetoothGatt!!.services.map { it.uuid }.joinToString("\n")
                Logger.d(services)

                mBluetoothGatt!!.services.map {
                    Logger.d("service: " + it.uuid)
                    it.characteristics.map {
                        Logger.d("characteristic: uuid-> ${it.uuid}\n ${it.properties}")
                    }
                }

                serv1530 = mBluetoothGatt!!.services.find { it.uuid == MyConfigBle.BLE_1530 }
                chara1531 = serv1530!!.getCharacteristic(MyConfigBle.BLE_1531)
//                chara1531!!.value = byteArrayOf(0,1,2,3)
//                chara1531!!.writeType = BluetoothGattCharacteristic.PROPERTY_WRITE
//                mBluetoothGatt!!.writeCharacteristic(chara1531)
                publishEventToUpdate(MsgBox.BLE_EVENT_CONNECTED)
            } else {
                Logger.e("onServicesDiscovered[fail] received: " + status)
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            Logger.d("onCharacteristicRead: status-> $status; characteristic-> ${characteristic.toString()}")
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            Logger.d("onCharacteristicChanged: ${characteristic.toString()}")
        }

        override fun onReadRemoteRssi(gatt: BluetoothGatt?, rssi: Int, status: Int) {
            Logger.d("onReadRemoteRssi: rssi-> $rssi, status-> $status")
        }
    }

    inner class BleBinder : Binder() {
        fun getService(): BleService = this@BleService
    }

    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    /**
     * 检查蓝牙初始化
     */
    fun initialize(): Boolean {
        if (mBleManager == null) {
            mBleManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            if (mBleManager == null) {
                Logger.d("Unable to initialize BluetoothManager.")
                return false
            }
        }

        mBleAdapter = mBleManager!!.adapter
        if (mBleAdapter == null) {
            Logger.d("Unable to initialize BluetoothManager.")
            return false
        }

        return true
    }

    // 扫描
    fun scanStart() {
        Logger.d("scan start ok...")
        postStopEnsure()
        MyBleSingleInfo.mDeviceList.clear()
        mIsScanning = true
        mBleAdapter!!.startLeScan(mLeScanCallback)
        publishEventToUpdate(BLE_UI_UPDATE_MENU)
    }

    fun scanStop() {
        if (mIsScanning) {
            Logger.d("scan stop ok...")
            mIsScanning = false
            mBleAdapter!!.stopLeScan(mLeScanCallback)
            publishEventToUpdate(BLE_UI_UPDATE_MENU)
            postStopCancel()
            publishEventToUpdate(BLE_EVENT_STOP_SCAN)
        }
    }

    private fun postStopEnsure() {
        handler.postDelayed(r, SCAN_PERIOD)
    }

    private fun postStopCancel() {
        handler.removeCallbacks(r)
    }

    /**
     * Ble scan callback
     */
    private val mLeScanCallback= BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
        Logger.d("scanned: ${device.name} ${device.address} $rssi")
        if (device.name != null && (device.name.toLowerCase().startsWith("me") || device.name.toLowerCase().contains("dfu") )) {
            val tmp = MyScannedDevice(device, rssi)

            val index = MyBleSingleInfo.mDeviceList.indexOf(tmp)
            if (index == -1) {
                MyBleSingleInfo.mDeviceList.add(tmp)
            } else {
                MyBleSingleInfo.mDeviceList[index].mRssi = rssi
            }
        }
    }

    /**
     * ble 普通方法
     */
    fun connect(address: String?): Boolean {
        if (mBleAdapter == null || address == null) {
            Logger.e("BluetoothAdapter not initialized or unspecified address.")
            return false
        }

        // Previously connected device.  Try to reconnect.
        if (MyBleSingleInfo.mAddress != null && address == MyBleSingleInfo.mAddress && mBluetoothGatt != null) {
            Logger.d("Trying to use an existing mBluetoothGatt for connection.")
            if (mBluetoothGatt!!.connect()) {
                MyBleSingleInfo.mConnectionState = MyConnectionEnum.STATE_CONNECTING
                return true
            } else {
                return false
            }
        }

        val device = mBleAdapter!!.getRemoteDevice(address)
        if (device == null) {
            Logger.e("Device not found.  Unable to connect.")
            return false
        }
        // We want to directly connect to the device, so we are setting the autoConnect parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback)

        Logger.d("Trying to create a new connection.")
        MyBleSingleInfo.mAddress = address
        MyBleSingleInfo.mName = mBluetoothGatt!!.device.name
        MyBleSingleInfo.mConnectionState = MyConnectionEnum.STATE_CONNECTING
        return true
    }

    fun disconnect() {
        if (mBleAdapter == null || mBluetoothGatt == null) {
            Logger.e("BluetoothAdapter not initialized")
            return
        }
        mBluetoothGatt!!.disconnect()
    }

    fun writeBle() {
        chara1531!!.value = byteArrayOf(1, 4)
        mBluetoothGatt!!.writeCharacteristic(chara1531)
    }

    fun setCharacteristicNotification(enabled: Boolean) {
        val characteristic = chara1531!!
        if (mBleAdapter == null || mBluetoothGatt == null) {
            Logger.e("BluetoothAdapter not initialized")
            return
        }

        mBluetoothGatt!!.setCharacteristicNotification(characteristic, enabled)
        val descriptor = characteristic.getDescriptor(MyConfigBle.CLIENT_CHARACTERISTIC_CONFIGURATION_UUID)
        if (descriptor != null) {
            // prefer notify over indicate
            when {
                characteristic.properties and BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0 ->
                    descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                characteristic.properties and BluetoothGattCharacteristic.PROPERTY_INDICATE != 0 ->
                    descriptor.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                else -> Logger.e("This characteristic does not have NOTIFY or INDICATE property set")
            }
            mBluetoothGatt!!.writeDescriptor(descriptor)
        }
    }


    /**
     * 广播UI的更新
     */
    private fun publishEventToUpdate(updateId: Int) {
        EventBus.getDefault().post(MsgBox(updateId))
    }

    // service 中常用的几个方法
//    override fun onCreate() {
//        super.onCreate()
//    }
//
//    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        return super.onStartCommand(intent, flags, startId)
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//    }

}


