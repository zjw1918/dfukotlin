package cn.zjw.dfukotlin

import android.app.Application
import android.content.Context
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger

/**
 * Created by lincoln on 17-8-29.
 */
class MyApplication : Application() {
    companion object {
        lateinit var instance: MyApplication
            private set
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
        Logger.addLogAdapter(AndroidLogAdapter())
//        Logger.addLogAdapter(DiskLogAdapter())
    }
}