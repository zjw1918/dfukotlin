package cn.zjw.dfukotlin.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

/**
 * Created by lincoln on 17-8-30.
 */
class UtilActivity {
    companion object {
        fun addFragmentToActivity(fragmentManager: FragmentManager, fragment: Fragment, frameId: Int) {
            checkNotNull(fragmentManager)
            checkNotNull(fragment)
            val transaction = fragmentManager.beginTransaction()
            transaction.add(frameId, fragment)
            transaction.commit()
        }
    }
}