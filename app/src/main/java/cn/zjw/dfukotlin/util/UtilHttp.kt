package cn.zjw.dfukotlin.util

import okhttp3.OkHttpClient
import okhttp3.Request

/**
 * Created by lincoln on 17-11-24.
 */
class UtilHttp {
    companion object {
        fun sendOkHttpRequest(url: String, callback: okhttp3.Callback) {
            val client = OkHttpClient()
            val request = Request.Builder().url(url).build()
            client.newCall(request).enqueue(callback)
        }
    }
}