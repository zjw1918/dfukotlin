package cn.zjw.dfukotlin

/**
 * Created by lincoln on 17-8-30.
 */
interface BasePresenter {
    fun start()
}