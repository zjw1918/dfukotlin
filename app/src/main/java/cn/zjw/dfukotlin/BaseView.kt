package cn.zjw.dfukotlin

/**
 * Created by lincoln on 17-8-30.
 */
interface BaseView<T> {
    fun setPresenter(presenter: T)
}