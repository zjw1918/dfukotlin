package cn.zjw.dfukotlin.eventsPojo

/**
 * Created by lincoln on 17-11-28.
 */
data class MsgBox(var msgId: Int) {
    companion object {
        val BLE_UI_UPDATE_MENU = 10000
        val BLE_EVENT_STOP_SCAN = 10001
        val BLE_EVENT_CONNECTED = 10002
        val BLE_EVENT_DISCONNECT = 10003

    }

    var devicePosition: Int? = null
}