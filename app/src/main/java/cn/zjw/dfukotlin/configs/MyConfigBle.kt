package cn.zjw.dfukotlin.configs

import java.util.*

/**
 * Created by lincoln on 17-12-1.
 */
class MyConfigBle {
    companion object {
        val BLE_FAB1 = UUID.fromString("0000fab1-0000-1000-8000-00805f9b34fb")
        val BLE_FAB2 = UUID.fromString("0000fab2-0000-1000-8000-00805f9b34fb")
        val BLE_FAB3 = UUID.fromString("0000fab3-0000-1000-8000-00805f9b34fb")
        val BLE_FAB4 = UUID.fromString("0000fab4-0000-1000-8000-00805f9b34fb")
        val BLE_FAB5 = UUID.fromString("0000fab5-0000-1000-8000-00805f9b34fb")
        val BLE_FAB6 = UUID.fromString("0000fab6-0000-1000-8000-00805f9b34fb")

        val BLE_1530 = UUID.fromString("00001530-1212-efde-1523-785feabcd123")
        val BLE_1531 = UUID.fromString("00001531-1212-efde-1523-785feabcd123")
        val BLE_1532 = UUID.fromString("00001532-1212-efde-1523-785feabcd123")
        val BLE_1533 = UUID.fromString("00001534-1212-efde-1523-785feabcd123")

        val CLIENT_CHARACTERISTIC_CONFIGURATION_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

    }
}