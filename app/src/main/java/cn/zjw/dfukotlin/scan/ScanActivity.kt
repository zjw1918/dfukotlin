package cn.zjw.dfukotlin.scan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar

import cn.zjw.dfukotlin.R
import cn.zjw.dfukotlin.util.UtilActivity
import org.jetbrains.anko.find

class ScanActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)

        val toolbar = find<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val ab = supportActionBar
        ab?.let {
            title = "扫描设备"
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        // 初始化view
        var scanFragment = supportFragmentManager.findFragmentById(R.id.contentFrame)
        if (scanFragment == null) {
            scanFragment = ScanFragment.newInstance()
            UtilActivity.addFragmentToActivity(supportFragmentManager,scanFragment, R.id.contentFrame)
        }

        // 初始化presenter
        ScanPresenter(scanFragment as ScanContract.View)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
