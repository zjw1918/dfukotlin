package cn.zjw.dfukotlin.scan

import com.orhanobut.logger.Logger

/**
 * Created by lincoln on 17-8-30.
 */
class ScanPresenter(private val mScanView: ScanContract.View): ScanContract.Presenter {
    init {
        mScanView.setPresenter(this)
    }

    override fun start() {
        Logger.d("ScanPresenter start ok")
//        mScanView.bindAdapter(mDeviceAdapter)
    }

    override fun enableScan(enable: Boolean) {

    }
    
}