package cn.zjw.dfukotlin.scan

import cn.zjw.dfukotlin.BasePresenter
import cn.zjw.dfukotlin.BaseView

/**
 * Created by lincoln on 17-8-30.
 */
interface ScanContract {
    interface Presenter: BasePresenter {
        fun enableScan(enable: Boolean)
    }

    interface View: BaseView<Presenter> {
        fun showScan(flag: Boolean)
//        fun bindAdapter(adapter: DeviceAdapter)
//        fun updateAdapter(adapter: DeviceAdapter)
    }
}