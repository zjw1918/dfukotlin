package cn.zjw.dfukotlin.scan

import android.annotation.SuppressLint
import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import cn.zjw.dfukotlin.R
import cn.zjw.dfukotlin.eventsPojo.MsgBox
import cn.zjw.dfukotlin.models.MyScannedDevice
import cn.zjw.dfukotlin.services.BleService
import cn.zjw.dfukotlin.services.MyBleSingleInfo
import com.orhanobut.logger.Logger
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import java.util.*

/**
 * Created by lincoln on 17-8-30.
 */
class ScanFragment: Fragment(), ScanContract.View {
    private var mPresenter: ScanContract.Presenter? = null
    private var recyclerView: RecyclerView? = null
    private var mBleService: BleService? = null
    private var timer: Timer? = null
    private var task: TimerTask? = null
    private var mDeviceAdapter: DeviceAdapter? = null

    companion object {
        val UPDATE_LIST = 10000

        fun newInstance() = ScanFragment()
    }

    private val handler = @SuppressLint("HandlerLeak")
    object: Handler() {
        override fun handleMessage(msg: Message?) {
            when (msg!!.what) {
                UPDATE_LIST -> {
                    mDeviceAdapter!!.notifyDataSetChanged()
                }
            }
            super.handleMessage(msg)
        }
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            mBleService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Logger.d("onServiceConnected")
            mBleService = (service as BleService.BleBinder).getService()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDeviceAdapter = DeviceAdapter(MyBleSingleInfo.mDeviceList)

        val bindServiceIntent = Intent(activity, BleService::class.java)
        activity.bindService(bindServiceIntent, connection, Service.BIND_AUTO_CREATE)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater?.inflate(R.layout.frag_scan, container, false)
        recyclerView = root?.find(R.id.recycler_view)
        recyclerView!!.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        recyclerView!!.adapter = mDeviceAdapter

        setHasOptionsMenu(true)
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.scan_toggle, menu)
        if (!mBleService!!.mIsScanning) {
            menu!!.findItem(R.id.menu_stop).isVisible = false
            menu.findItem(R.id.menu_scan).isVisible = true
            menu.findItem(R.id.menu_refresh).actionView = null
        } else {
            menu!!.findItem(R.id.menu_stop).isVisible = true
            menu.findItem(R.id.menu_scan).isVisible = false
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_scan -> {
                mBleService!!.scanStart()
                startUpdateListTimer()
                mDeviceAdapter!!.notifyDataSetChanged()
            }
            R.id.menu_stop -> {
                mBleService!!.scanStop()
                stopUpdateListTimer()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setPresenter(presenter: ScanContract.Presenter) {
        mPresenter = presenter
    }

    override fun showScan(flag: Boolean) {

    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter?.enableScan(false)
        mBleService!!.scanStop()
        activity.unbindService(connection)
        MyBleSingleInfo.mDeviceList.clear()
        stopUpdateListTimer()
    }

    inner class DeviceAdapter(var deviceList: List<MyScannedDevice>): RecyclerView.Adapter<ViewHolder>() {
        override fun getItemCount(): Int = deviceList.size

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(activity).inflate(R.layout.item_device_scan, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
            val device = deviceList[position]
            holder!!.bindView(device)
        }
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var view: CardView = itemView as CardView
        var tvName: TextView = view.find(R.id.tv_name)
        var tvMac: TextView = view.find(R.id.tv_mac)
        var tvRssi: TextView = view.find(R.id.tv_rssi)

        private var mDevice: MyScannedDevice? = null

        fun bindView(device: MyScannedDevice) {
            tvName.text = device.mDevice.name
            tvMac.text = device.mDevice.address
            tvRssi.text = device.mRssi.toString()
            mDevice = device

            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            activity.toast("正在连接: " + mDevice!!.mDevice.address)
            mBleService!!.connect(mDevice!!.mDevice.address)

        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    /**
     * EventBus subscribe
     */
    @Subscribe
    fun onMsgBox(e: MsgBox) {
        when (e.msgId) {
            MsgBox.BLE_UI_UPDATE_MENU -> {
                activity.invalidateOptionsMenu()
            }
            MsgBox.BLE_EVENT_STOP_SCAN -> {
                stopUpdateListTimer()
            }
        }
    }

    /**
     * timer task
     */
    private fun startUpdateListTimer() {
        timer = Timer()
        task = object: TimerTask() {
            override fun run() {
                handler.sendEmptyMessage(UPDATE_LIST)
            }
        }
        timer!!.schedule(task, 500, 1 * 1000)
    }

    private fun stopUpdateListTimer() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }
}