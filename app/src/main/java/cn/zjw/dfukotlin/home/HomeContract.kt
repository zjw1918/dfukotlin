package cn.zjw.dfukotlin.home

import cn.zjw.dfukotlin.BasePresenter
import cn.zjw.dfukotlin.BaseView

/**
 * Created by lincoln on 17-11-24.
 */
interface HomeContract {
    interface Presenter: BasePresenter {
        fun loadDeviceInfo()
    }

    interface View : BaseView<Presenter> {
        fun updateUI()
    }
}