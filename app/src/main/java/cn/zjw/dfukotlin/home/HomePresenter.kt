package cn.zjw.dfukotlin.home

import com.orhanobut.logger.Logger

/**
 * Created by lincoln on 17-11-24.
 */
class HomePresenter(private val homeView: HomeContract.View): HomeContract.Presenter {

    init {
        homeView.setPresenter(this)
    }

    override fun start() {
        Logger.d("HomePresenter start")
    }

    override fun loadDeviceInfo() {
        homeView.updateUI()
    }

}