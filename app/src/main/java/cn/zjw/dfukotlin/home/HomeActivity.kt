package cn.zjw.dfukotlin.home

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import cn.zjw.dfukotlin.R
import cn.zjw.dfukotlin.scan.ScanActivity
import cn.zjw.dfukotlin.util.UtilActivity
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast


class HomeActivity : AppCompatActivity() {
    private val REQUEST_ENABLE_BT = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = find<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

//        if (!BleManager._isBleEnable) {
//            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
//            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
//        }

        // 初始化view
        var homeFragment = supportFragmentManager.findFragmentById(R.id.contentFrame)
        if (homeFragment == null) {
            homeFragment = HomeFragment.newInstance()
            UtilActivity.addFragmentToActivity(supportFragmentManager,homeFragment, R.id.contentFrame)
        }

        HomePresenter(homeFragment as HomeContract.View)

//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.BLUETOOTH), REQUEST_ENABLE_BT)
//        } else {
//            Logger.d("ble ok")
//        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_ENABLE_BT -> {
                if (resultCode == Activity.RESULT_OK) {
                    toast("蓝牙可用")
                } else {
                    toast("蓝牙不可用")
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.open_scan -> {
                startActivity<ScanActivity>()
            }
        }
        return super.onOptionsItemSelected(item)
    }

//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        when (requestCode) {
//            REQUEST_ENABLE_BT -> {
//                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    toast("蓝牙已经授权可用")
//                } else {
//                    toast("蓝牙授权失败")
//                }
//            }
//        }
//    }
}
