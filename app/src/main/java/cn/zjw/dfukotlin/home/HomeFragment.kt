package cn.zjw.dfukotlin.home


import android.app.Activity
import android.app.LoaderManager
import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import cn.zjw.dfukotlin.R
import cn.zjw.dfukotlin.eventsPojo.MsgBox
import cn.zjw.dfukotlin.services.BleService
import cn.zjw.dfukotlin.services.MyBleSingleInfo
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.frag_home.*
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter
import no.nordicsemi.android.dfu.DfuServiceInitiator
import no.nordicsemi.android.dfu.DfuServiceListenerHelper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import java.io.File


class HomeFragment : Fragment(), HomeContract.View, View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    companion object {
        private val REQUEST_ENABLE_BT = 1
        private val TARGET_MAC = "BC:E5:9F:48:75:2D"
//        private val TARGET_MAC = "F7:5B:2E:9B:BF:28"
        private val TARGET_NAME = "DfuTarg"
        private val SELECT_FILE_REQ = 2
        private val EXTRA_URI = "uri"

        fun newInstance(): HomeFragment = HomeFragment()
    }

    private var mPresenter: HomeContract.Presenter? = null
    private var mDevicePlaceholder: TextView? = null
    private var mMac: TextView? = null
    private var mBtnStartDfu: Button? = null
    private var mBtnSelectedFile: Button? = null
    private var mBtnOpenListen: Button? = null
    private var mBtnWriteTo: Button? = null



    private val mFileType = DfuService.TYPE_AUTO
    private var mFilePath: String? = null
    private var mFileStreamUri: Uri? = null

    private var mBleServie: BleService? = null
    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            mBleServie = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Logger.d("onServiceConnected")
            mBleServie = (service as BleService.BleBinder).getService()
            if (!mBleServie!!.initialize()) {
                activity.toast("无法初始化蓝牙")
                return
            }
            if (!mBleServie!!.mBleAdapter!!.isEnabled) {
                if (!mBleServie!!.mBleAdapter!!.isEnabled) {
                    val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
                }
            }

            updateUI()
        }
    }

    override fun setPresenter(presenter: HomeContract.Presenter) {
        mPresenter = presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bindServiceIntent = Intent(activity, BleService::class.java)
        activity.bindService(bindServiceIntent, connection, Service.BIND_AUTO_CREATE)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.frag_home, container, false)
        mDevicePlaceholder = view.find(R.id.no_device_hint)
        mMac = view.find(R.id.mac)
        mBtnStartDfu = view.find(R.id.start_dfu)
        mBtnSelectedFile = view.find(R.id.select_file)
        mBtnOpenListen = view.find(R.id.openListen)
        mBtnWriteTo = view.find(R.id.writeTo)


        mBtnStartDfu!!.setOnClickListener(this)
        mBtnSelectedFile!!.setOnClickListener(this)
        mBtnOpenListen!!.setOnClickListener(this)
        mBtnWriteTo!!.setOnClickListener(this)

        return view
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.start_dfu -> {
                startDfu()
            }
            R.id.select_file -> {
                onBtnClickSelectFile()
            }
            R.id.openListen -> {
                mBleServie!!.setCharacteristicNotification(true)
            }
            R.id.writeTo -> {
                mBleServie!!.writeBle()
            }
        }
    }

    private fun onBtnClickSelectFile() {
        AlertDialog.Builder(activity).setTitle("选择文件:")
                .setPositiveButton(android.R.string.ok, { dialog, which -> openFileChooser() })
                .setNegativeButton(android.R.string.cancel, null)
                .show()
    }

    private fun startDfu() {
        mDevicePlaceholder!!.text = "__"
        Logger.d("original dfu mac: " + MyBleSingleInfo.mAddress!!)
        var macTmp =  MyBleSingleInfo.mAddress!!
        if (!MyBleSingleInfo.mName!!.toLowerCase().contains("dfu")) {
            macTmp = getDfuMac(MyBleSingleInfo.mAddress!!)
        }
        Logger.d("new dfu mac: " + macTmp)

        val starter = DfuServiceInitiator(macTmp)
                .setDeviceName(TARGET_NAME)
                .setKeepBond(false)
        starter.setUnsafeExperimentalButtonlessServiceInSecureDfuEnabled(true)
        starter.setZip(mFileStreamUri, mFilePath)
        val controller = starter.start(activity, DfuService::class.java)
    }

    private fun openFileChooser() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = DfuService.MIME_TYPE_ZIP
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        if (intent.resolveActivity(activity.packageManager) != null) {
            // file browser has been found on the device
            startActivityForResult(intent, SELECT_FILE_REQ)
        } else {
            // there is no any file browser app, let's try to download one
            activity.toast("没有文件浏览器")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_ENABLE_BT -> {
                if (resultCode == Activity.RESULT_CANCELED) {
                    activity.toast("用户禁止了蓝牙")
                }
            }

            SELECT_FILE_REQ -> {
                if (resultCode == Activity.RESULT_OK) {
                    // clear previous data
                    mFilePath = null
                    mFileStreamUri = null

                    // and read new one
                    val uri = data!!.data
                    /*
                     * The URI returned from application may be in 'file' or 'content' schema. 'File' schema allows us to create a File object and read details from if
                     * directly. Data from 'Content' schema must be read by Content Provider. To do that we are using a Loader.
                     */
                    if (uri!!.scheme == "file") {
                        // the direct path to the file has been returned
                        val path = uri.path
                        val file = File(path)
                        mFilePath = path

                        Logger.d("Chosen file: ${file.name}, ${file.length()}, $mFileType")
                    } else if (uri.scheme == "content") {
                        // an Uri has been returned
                        mFileStreamUri = uri
                        // if application returned Uri for streaming, let's us it. Does it works?
                        // FIXME both Uris works with Google Drive app. Why both? What's the difference? How about other apps like DropBox?
                        val extras = data.extras
                        if (extras != null && extras.containsKey(Intent.EXTRA_STREAM))
                            mFileStreamUri = extras.getParcelable(Intent.EXTRA_STREAM)

                        // file name and size must be obtained from Content Provider
                        val bundle = Bundle()
                        bundle.putParcelable(EXTRA_URI, uri)
                        activity.loaderManager.restartLoader(SELECT_FILE_REQ, bundle, this)
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onResume() {
        super.onResume()
        updateUI()
        DfuServiceListenerHelper.registerProgressListener(activity, mDfuProgressListener)
    }

    override fun onPause() {
        super.onPause()
        DfuServiceListenerHelper.unregisterProgressListener(activity, mDfuProgressListener);
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mBleServie != null) {
            mBleServie!!.disconnect()
            activity.unbindService(connection)
        }
    }

    /**
     * EventBus Subscribe
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMsgBox(e: MsgBox) {
        Logger.d("onBleMessageEvent: ", e)
        if (e.msgId == MsgBox.BLE_EVENT_DISCONNECT || e.msgId == MsgBox.BLE_EVENT_CONNECTED) {
            updateUI()
        }
    }

    override fun updateUI() {
        if (mBleServie == null) return
        if (MyBleSingleInfo.mAddress == null) {
//            mDevicePlaceholder!!.visibility = View.VISIBLE
            mMac!!.text = null
        } else {
//            mDevicePlaceholder!!.visibility = View.INVISIBLE
            mMac!!.text = MyBleSingleInfo.mAddress
        }
    }

    /**
     * load chosen file
     */
    override fun onLoaderReset(p0: Loader<Cursor>?) {
        mFilePath = null
        mFileStreamUri = null
    }

    override fun onCreateLoader(p0: Int, args: Bundle?): Loader<Cursor> {
        val uri = args!!.getParcelable<Uri>(EXTRA_URI)
        /*
		 * Some apps, f.e. Google Drive allow to select file that is not on the device. There is no "_data" column handled by that provider. Let's try to obtain
		 * all columns and than check which columns are present.
		 */
        // final String[] projection = new String[] { MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.MediaColumns.SIZE, MediaStore.MediaColumns.DATA };
        return CursorLoader(activity, uri, null, null, null, null)

    }

    override fun onLoadFinished(p0: Loader<Cursor>?, data: Cursor?) {
        if (data != null && data.moveToNext()) {
            /*
			 * Here we have to check the column indexes by name as we have requested for all. The order may be different.
			 */
            val fileName = data.getString(data.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME)/* 0 DISPLAY_NAME */)
            val fileSize = data.getInt(data.getColumnIndex(MediaStore.MediaColumns.SIZE) /* 1 SIZE */)
            var filePath: String? = null
            val dataIndex = data.getColumnIndex(MediaStore.MediaColumns.DATA)
            if (dataIndex != -1)
                filePath = data.getString(dataIndex /* 2 DATA */)
            if (!TextUtils.isEmpty(filePath))
                mFilePath = filePath

//            updateFileInfo(fileName, fileSize.toLong(), mFileType)
            Logger.d("onLoadFinished $fileName, ${fileSize.toLong()}, $mFileType")
        } else {
            mFilePath = null
            mFileStreamUri = null
        }
    }

    val mDfuProgressListener = object: DfuProgressListenerAdapter() {
        override fun onProgressChanged(deviceAddress: String?, percent: Int, speed: Float, avgSpeed: Float, currentPart: Int, partsTotal: Int) {
            Logger.d("onProgressChanged: $percent")
            mDevicePlaceholder!!.text = "$percent%"
        }

        override fun onError(deviceAddress: String?, error: Int, errorType: Int, message: String?) {
            Logger.e("onError:" + message)

        }

        override fun onDfuCompleted(deviceAddress: String?) {
            Logger.d("onDfuCompleted")
        }
    }

    fun getDfuMac(address: String): String {
        val arr = address.split(":").toMutableList()
        val b = arr.last().toInt(radix = 16) + 1
        val final = b.toString(16)
        arr[arr.lastIndex] = (if (final.length >= 2) final else "0" + final).toUpperCase()
        return arr.joinToString(":")
    }
}
